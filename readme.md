# Tutorial Flask

- [Tutorial Flask](#tutorial-flask)
  - [Instalar Flask](#instalar-flask)
  - [Añadimos `requirements.txt`](#añadimos-requirementstxt)
  - [Iniciamos aplicación Flask](#iniciamos-aplicación-flask)
  - [Request y Response](#request-y-response)
  - [Templates con Jinja 2](#templates-con-jinja-2)
    - [Estructuras de control](#estructuras-de-control)
    - [Extensión, Herencia de templates](#extensión-herencia-de-templates)
    - [Macros](#macros)
    - [Include y links](#include-y-links)
  - [Uso de archivos estáticos: imágenes](#uso-de-archivos-estáticos-imágenes)
  - [Configurando paǵinas de error](#configurando-paǵinas-de-error)
  - [Estilos Bootstrap en Flask](#estilos-bootstrap-en-flask)
    - [Instalando Flask-Bootstrap](#instalando-flask-bootstrap)
  - [Configurar Flask](#configurar-flask)
  - [Formularios con WTF y Bootstrap](#formularios-con-wtf-y-bootstrap)
    - [Creación de formularios con Flask-WTF](#creación-de-formularios-con-flask-wtf)
    - [Renderización de formularios con Flask-Bootstrap](#renderización-de-formularios-con-flask-bootstrap)
    - [Uso de método POST con Flask-WTF](#uso-de-método-post-con-flask-wtf)
  - [Filtros Jinja](#filtros-jinja)
  - [Mostrar mensajes al usuario, Flashing](#mostrar-mensajes-al-usuario-flashing)
  - [Pruebas básicas con Flask-Testing](#pruebas-básicas-con-flask-testing)

[**Flask**](https://palletsprojects.com/p/flask/) es un *microframework* para aplicaciones web escritas en python. Permite crear aplicaciones web rápidamente y con un mínimo de líneas de código. Puedes extender sus funcionalidades con las llamadas ***Flask Extensions***.

## Instalar Flask

Asegurate de tener python3 instalado y las herramientas para crear entornos virtuales de trabajo, `virtualenv` y `virtualenvwrapper`.

1. Instalar `virtualenv` y `virtualenvwrapper`
2. Crear entorno virtual con `mkvirtualenv`
3. Moverse al directorio de trabajo y activar el entorno con `workon flask_dev`

```zsh
~/workspace/cursos/Flask on  master! ⌚ 19:49:19
$ mkvirtualenv --python=/usr/bin/python3 flask_dev
$ workon flask_dev
```

Una vez estamos en el entorno virtual de trabajo instalamos `flask` con `pip`.

```zsh
(flask_dev):$ pip install flask  
```

Con `pip freeze` vemos los paquetes instalados.

## Añadimos `requirements.txt`

Creamos un archivo con el nombre `requirements.txt` y escribimos en el en la primera línea `flask`.

Con esto en el futuro podemos instalar todas las dependencias que necesitemos con:

```zsh
(flask_dev):$ pip install -r requirements.tx  
```

## Iniciamos aplicación Flask

Creamos archivo `main.py` donde importaremos `flask` y creamos la `app`. El archivo `main.py` queda como sigue:

```python
# main.py

from flask import Flask

app = Flask(__name__)
```

Creamos una función para que nos muestre el típico *"Hello World from Flask"*. Usaremos el decorador `route` para convertir la función en una ruta. Ahora queremos que aparezca en la raíz (`'/'`) de nuestra aplicación.

```python
# main.py

from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello World from Flask"
```

Necesitamos un servidor web para poder probar que esto funciona. Eso lo conseguimos sin más que ejecutar el comando `flask run`.

```zsh
(flask_dev):$ flask run
```

Nos da un error porque no hemos configurado la variable de entorno `FLASK_APP=main.py`. Esto lo podemos realizar desde la línea de comandos con `export FLASK_APP=main.py`. Pero esto desaparecerá cuando desactivemos el entorno virtual o cerremos la consola.

Lo correcto es colocarlo en nuestro entorno virtual para que cada vez que lo activemos nos configure esta variable. Para ello usaremos el archivo `postactivate` de nuestro entorno virtual. Tenemos que localizar el directorio de nuestro entorno virtual y editar el archivo.

```zsh
(flask_dev):$ nano ~/.virtualenvs/flask_dev/bin/postactivate
```

Añadimos la línea al archivo `postactivate`:

```zsh
#!/bin/zsh
# This hook is sourced after this virtualenv is activated.

export FLASK_APP="main.py"
```

Ahora si, ejecutemos el comando `flask run` y veamos el efecto en el navegador. La salida del comando nos dirá en que dirección y puerto esta escuchando el servidor web.

```zsh
$ flask run
 * Serving Flask app "main.py" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production enviroment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Cada vez que cambiemos algo tenemos que iniciar el servidor y ademas si tenemos errores no serán mostrados en el navegador. Observa que la salida del comando `flask run` nos dice que el `Debug mode: off`. Para ponerlo en `on` tenemos que configurar una nueva variable de entorno `FLASK_DEBUG=1` y que pondremos igual que hicimos anteriormente en nuestro archivo `postactivate`.

```zsh
#!/bin/zsh
# This hook is sourced after this virtualenv is activated.

export FLASK_APP=main.py
export FLASK_DEBUG=1
```

## Request y Response

Flask nos proporciona un objeto `request` que almacena diferentes variables de de la sesión web, entre ellas la dirección ip del usuario remoto que se conecta. Importaremos el objeto `request` y podemos usarlo en la siguiente forma.

```python
# main.py

from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello():
    user_ip = request.remote_addr
    return f"Hello World from Flask, tu IP es {user_ip}"
```

Vamos a cambiar un poco nuestra estructura. modificamos nuestro punto de entrada (el root `'/'`), ahora será `index`. Aquí recogemos la dirección del usuario remoto, guardándola en una *cookie* y generaremos una respuesta de redirección a `'/hello'`. En esta nueva dirección leeremos la cookie que hemos creado antes y la mostramos.

```python
# main.py

from flask import Flask, request, make_response, redirect

app = Flask(__name__)


@app.route('/')
def index():
    user_ip = request.remote_addr

    response = make_response(redirect('/hello'))
    response.set_cookie('user_ip', user_ip)

    return response


@app.route('/hello')
def hello():
    user_ip = request.cookies.get('user_ip')
    return f"Hello World from Flask {user_ip}"
```

## Templates con Jinja 2

Flask depende de Jinja2, un motor de plantillas con todas las funciones para Python. Tiene soporte completo para *Unicode*, un entorno de ejecución *sandboxed* integrado opcional, ampliamente utilizado y con licencia BSD. Un template es un archivo para formatear `html` y variables estáticas y dinámicas. Flask espera encontrar todos los templates en un directorio `templates`. Ahí crearemos nuestro primer template `hello.html`.

### Estructuras de control

En los templates podemos crear estructuras de control para renderizar el contenido del `html` en función del contenido de una variable. El siguiente template utiliza la variable `user_ip` para cambiar el código en caso de no tener contenido. Así quedaría el contenido del archivo `hello.html`.

```jinja
<!-- hello.html -->
{% if user_ip %}
    <h1>Hello World from Flask, tu IP es {{ user_ip }} </h1>
{% else %}
    <a href="{{ url_for('index') }}">Ir a inicio</a>
{% endif %}
```

También podemos usar estructuras de control para recorrer variables, como una lista de tareas. Como estamos ampliando el número de variables que vamos a pasar al *render* crearemos un contexto donde agruparemos las variables a pasar. Este contexto será un diccionario de `python` y se lo pasaremos a la función `render_template` como un diccionario expandido, `**context`.

```python
<todos = ['TODO 1', 'TODO 2', 'TODO 3', 'TODO 4']

...

@app.route('/hello')
def hello():
    user_ip = request.cookies.get('user_ip')
    context = {
        'user_ip': user_ip,
        'todos': todos
    }
    return render_template('hello.html', **context)
```

Y nuestro template quedara como sigue.

```jinja
<!-- hello.html -->
{% if user_ip %}
    <h1>Hello World from Flask, tu IP es {{ user_ip }} </h1>
{% else %}
    <a href="{{ url_for('index') }}">Ir a inicio</a>
{% endif %}

<ul>
    {% for todo in todos %}
        <li>{{ todo }}</li>
    {% endfor %}
</ul>
```

### Extensión, Herencia de templates

Crearemos un template base que será como el contenedor que acogerá diferentes templates para modificar o completar la página que queremos crear. Nuestra base será `base.html`. Este archivo contendrá la estructura general.

En el archivo `hello.html` colocamos al principio `{% extends 'base.html' %}` para que coloque primero la estructura general. La estructura general debe contener la definición de los bloques que van a ser sustituidos y en el lugar en el que estén definidos.

```jinja
<!-- base.html -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{% block title %} Flask Web | {% endblock title %}</title>
</head>
<body>
    {% block content %}
    {% endblock content %}
</body>
</html>
```

Observa que en el documento `base.html` hemos creado dos bloques, uno de título y otro para el contenido. El bloque de título ya tiene contenido y queremos que cuando se sustituya por otro, en realidad se añada primero ell contenido que tiene y luego el contenido del temple desde el que extendemos. Eso lo conseguimos con `{{ super() }}` que debe ser añadido antes del contenido del bloque que queremos incluir.

Ahora el template `hello.html` queda como sigue.

```jinja
<!-- hello.html -->
{% extends 'base.html' %}

{% block title %}{{ super() }} Bienvenido{% endblock title %}

{% block content %}
    {% if user_ip %}
        <h1>Hello World from Flask, tu IP es {{ user_ip }} </h1>
    {% else %}
        <a href="{{ url_for('index') }}">Ir a inicio</a>
    {% endif %}

    <ul>
        {% for todo in todos %}
            <li>{{ todo }}</li>
        {% endfor %}
    </ul>
{% endblock content %}
```

### Macros

Hay mucho código que puede ser reutilizado y podemos agruparlo como trozos de código en macros. De esta forma el template queda más límpio y claro y el código es más fácilmente mantenible.

Los macros se guardan como templates y por tanto en la misma carpeta. Su nombre será `macro.html`, y se definen como.

```jinja
<!-- macros.html -->
{% macro render_todo(todo) %}
    <li>Descripción: {{ todo }}</li>
{% endmacro %}
```

Primero hay que importar el archivo de macros en el template antes de poder utilizarlo. La importación es al estilo **Python** pero como una claúsula `jinja`.

```jinja
<!-- hello.html -->
{% extends 'base.html' %}
{% import 'macros.html' as macros  %}

{% block title %}{{ super() }} Bienvenido{% endblock title %}

{% block content %}
    {% if user_ip %}
        <h1>Hello World from Flask, tu IP es {{ user_ip }} </h1>
    {% else %}
        <a href="{{ url_for('index') }}">Ir a inicio</a>
    {% endif %}

    <ul>
        {% for todo in todos %}
            {{ macros.render_todo(todo) }}
        {% endfor %}
    </ul>
{% endblock content %}
```

Ahora la macro `render_todo()` está disponible para todos los templates que necesiten usarla.

### Include y links

Vamos a ver como manjar los enlaces en los templates para redirigir a los lugares o funciones apropiadas. Para ello crearemos una barra de navegación como un nuevo template.

```jinja
<!-- navbar.html -->
<nav>
    <ul>
        <li><a href="{{ url_for('index') }}">Inicio</a></li>
        <li><a href="https://palletsprojects.com/p/flask/">Flask</a></li>
    </ul>
</nav>
```

La barra de navegación estará en todas las páginas, por tanto, hay que incluirla en el elemento `base.html` y lo colocaremos en esta ocasión en el `header`.

```jinja
<!-- base.html -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{% block title %} Flask Web | {% endblock title %}</title>
</head>
<body>
    <header>
        {% include 'navbar.html' %}
    </header>
    {% block content %}
    {% endblock content %}
</body>
</html>
```

## Uso de archivos estáticos: imágenes

En cualquier proyecto web necesitamos archivos estáticos como imágenes, hojas de estilo, etc. Estos los colocaremos en un directorio denominado `static`. Dentro de él tendremos otros directorios para imágenes, hojas css, etc.

Añadimos un logo en el navegador y una hoja de estilos en el base.

```jinja
<!-- navbar.html -->
<nav>
    <ul>
        <img src="{{ url_for('static', filename='images/CitiLAN_Logo_FF7043.png') }}" alt="CitiLAN Logo">
        <li><a href="{{ url_for('index') }}">Inicio</a></li>
        <li><a href="https://palletsprojects.com/p/flask/">Flask</a></li>
    </ul>
</nav>
```

El archivo `base.html` quedara como sigue.

```jinja
<!-- base.html -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{% block title %} Flask Web | {% endblock title %}</title>
    <link rel="stylesheet" href="{{ url_for('static', filename='css/main.css') }}">
</head>
...
```

## Configurando paǵinas de error

Manejaremos errores. Cuando el usuario escribe una dirección para la cual no tenemos configurada una respuesta, el servidor lanza un error que el navegador interpreta de una forma estándar pero nosotros podemos crear una respuesta para estos casos.

Flask nos proporciona una función `errorhandler()` para capturar los diferentes errores y redirigir a una respuesta de nuestra elección.

```python
...
todos = ['Comprar cafe',
         'Revisar bicicleta',
         'Reordenar biblioteca',
         'Llevar coche al taller']


@app.errorhandler(404)
def not_found(error):
    return render_template('error.html', error=error)


@app.errorhandler(500)
def error_500(error):
    return render_template('error.html', error=error)

...
```

Como puedes ver se consigue mediante el decorador `@app.errorhandler(404)`, al que le pasamos el número de error. Luego la función que recogerá el error y redirige a la página preparada para manejar los errores.

La página a mostrar será un nuevo template como el que sigue.

```jinja
<!-- error.html -->
{% extends 'base.html' %}

{% block title %}{{ super() }} 404{% endblock title %}

{% block content %}
<h1>¡Algo salio mal!</h1>
<h2>Se produjo un Error en el servidor</h2>
<p>{{ error }}</p>
{% endblock content %}
```

## Estilos Bootstrap en Flask

Con Flask tu puedes usar tus propios estilos css o usar un de los múltiples frameworks css, como Materialize, Foundation o Boostrap. Existe una extensión para Bootstrap que te facilita el trabajo de integrar los estilos y ademas te proporciona algunas utilidades.

### Instalando Flask-Bootstrap

Vamos ha recoger este requerimiento en nuestro archivo `requirements.txt` para facilitar en el futuro su instalación. Incluimos una línea en el archivo con el texto, `flask-bootstrap` e instalamos con el comando `pip install -r requirements.txt`.

Una vez instalado tenemos que importarlo en nuestra aplicación y activarlo.

```python
from flask import Flask, request, make_response, redirect, render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)

...
```

Tenemos que modificar nuestro `base.html` para adaptarlo al base de `Flask-Bootstrap`.

```jinja
<!-- base.html -->
{% extends 'bootstrap/base.html' %}
{% block title %} Flask Web | {% endblock title %}

{% block head %}
    {{ super() }}
    {% block styles %}
        {{ super() }}
        <link rel="stylesheet" href="{{ url_for('static', filename='css/main.css') }}">
    {% endblock styles %}
{% endblock head %}

{% block body %}
    {% block navbar %}
        {% include 'navbar.html' %}
    {% endblock navbar %}

    {% block content %}{% endblock content %}
{% endblock body %}
```

Observa como nuestro `base.html`, ahora se extiende desde el `base.html` de `Flask-Bootstrap`. Añadimos nuestros bloques para el título y los bloques de `head` y `styles` para agregar nuestro archivo de estilos. Luego en los bloques de `body`, `navbar` y `content`.

Tenemos a nuestra disposición todos los estilos y componentes de Bootstrap. También puedes instalar `flask-materialize` que funciona de forma similar y permite trabajar con MaterializeCSS.

## Configurar Flask

Al principio durante la instalación creamos las variables FLASK_APP y FLASK_DEBUG. Otra variable que debemos configurar es `FLASK_ENV=development`. Esta variable la pondremos igualmente en el archivo `postactivate`, como hicimos con las otras.

```zsh
#!/bin/zsh
# This hook is sourced after this virtualenv is activated.

export FLASK_APP=main.py
export FLASK_DEBUG=1
export FLASK_ENV=development
```

Actualmente estamos guardando la cookie de la ip del usuario en formato texto plano. Imagina que además estuvieras guardando otro tipo de información que podría ser sensible si alguien consigue hacerse con ella. Incluso se podría modificar fácilmente. Otra forma es crear una llave secreta para encriptar la información y guardarla en el objeto sesión.

Desde el código podemos acceder a la configuración mediante `app.config`. Este objeto guarda varios valores entre ellos `SECRET_KEY`. Vamos a ver como usar este parámetro de la configuración.

```python
from flask import Flask, request, make_response, redirect, render_template, session
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)

app = Flask(__name__)
bootstrap = Bootstrap(app)

app.config['SECRET_KEY'] = 'CLAVE SECRETA'

...

@app.route('/')
def index():
    user_ip = request.remote_addr

    response = make_response(redirect('/hello'))
    # response.set_cookie('user_ip', user_ip)
    session['user_ip'] = user_ip

    return response


@app.route('/hello')
def hello():
    # user_ip = request.cookies.get('user_ip')
    user_ip = session.get('user_ip')
    context = {
        'user_ip': user_ip,
        'todos': todos
    }
    return render_template('hello.html', **context)

```

Importamos `session` y modificamos los `set_cookie` y `cookies.get()` para usar `session['user_ip']` y `session.get('user_ip')`. Ahora la ip del usuario se guarda encriptada.

## Formularios con WTF y Bootstrap

Instalemos la extensión Flask-WTF. Añadamos previamente a nuestro archivo de requerimientos la línea `flask-wtf`.

### Creación de formularios con Flask-WTF

Una vez instalado, necesitamos definir nuestro formulario como una clase que hereda de FlaskForm y que contiene los campos que constituyen el formulario y sus validadores. WTF tiene todos los validadores necesarios. También es necesario importar los tipos de campos que usamos. La clase para un formulario sencillo que recoja el nombre de usuario, su password y un botón para enviar se definiría de la siguiente forma.

```python
from wtforms.fields import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired

...

class LoginForm(FlaskForm):
    username = StringField('Nombre de usuario', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Enviar')

...
```

Una vez definida la clase que define el formulario, tenemos que añadirlo al contexto para poder enviarlo en el correspondiente `render_template`.

```python

...

@app.route('/hello')
def hello():
    user_ip = session.get('user_ip')
    login_form = LoginForm()
    context = {
        'user_ip': user_ip,
        'todos': todos,
        'login_form': login_form
    }
    return render_template('hello.html', **context)
```

Ahora podemos hacer uso de él en nuestro template `hello.html`, en el que definimos el formulario y sus estilos. Algo sencillo sería añadir el siguiente código.

```jinja
<!-- hello.html -->

...

        <div class="row">
            {% if user_ip %}
            <h1 class="display-4">Hello World from Flask, tu IP es {{ user_ip }} </h1>
            {% else %}
            <a href="{{ url_for('index') }}">Ir a inicio</a>
            {% endif %}
        </div>

        <div class="row">
            <div class="container">
                <form action="{{ url_for('hello') }}" method="POST">
                    <div class="form-group">
                        {{ login_form.username.label }}
                        {{ login_form.username }}
                    </div>
                    <div class="form-group">
                        {{ login_form.password.label }}
                        {{ login_form.password }}
                    </div>
                </form>
            </div>
        </div>
...
```

### Renderización de formularios con Flask-Bootstrap

Ya que tenemos instalado Flask-Bootstrap podemos aprovechar las facilidades que nos aporta para renderizar formularios en nuestros templates. Sustituimos el código anterior del template `hello.html`, en lo que respecta a la renderización del formulario por el código `{{ wtf.quick_form(login_form) }}`. Previamente debemos importar dentro del template el template de formularios de Flask-Bootstrap, `{% import 'bootstrap/wtf.html' as wtf %}`.

```jinja
<!-- hello.html -->

...

{% import 'bootstrap/wtf.html' as wtf %}

...
        <div class="row">
            <div class="container">
                {{ wtf.quick_form(login_form) }}
            </div>
        </div>
...
```

Ya tenemos nuestro formulario creado y con diseño y validación. Nos falta darle vida mediante un método `POST` que envíe efectivamente los datos al servidor y obtener una expuesta.

### Uso de método POST con Flask-WTF

Si pulsamos el botón de enviar nos devuelve un error. Nuestra ruta `/hello` la hemos definido por defecto, esto es, solo admite el método `GET`. La aceptación de métodos en la ruta se realiza mediante la adicción del parámetro `methods=[]` como una lista que recoge los métodos admitidos.

```python

...

@app.route('/hello')
def hello():
    user_ip = session.get('user_ip')
    login_form = LoginForm()
    context = {
        'user_ip': user_ip,
        'todos': todos,
        'login_form': login_form
    }
    return render_template('hello.html', **context)
```

Observa que tan solo por el hecho de admitir el método `POST`, ya no tenemos error al enviar el formulario pero en realidad no hemos enviado nada. Es necesario procesar el formulario. Previamente debemos comprobar que el formulario ha sido validado.

```python

...

@app.route('/hello', methods=['POST', 'GET'])
def hello():
    user_ip = session.get('user_ip')
    login_form = LoginForm()
    username = session.get('username')

    context = {
        'user_ip': user_ip,
        'todos': todos,
        'login_form': login_form
    }

    if login_form.validate_on_submit():
        username = login_form.username.data
        session['username'] = username

    return render_template('hello.html', **context)
```

Observa que comprobamos si el formulario es válido, en cuyo caso, obtenemos el username del formulario y lo guardamos en los datos de sesión. De este modo lo tenemos disponible en los datos de sesión. Podremos saber si el usuario se ha logueado o no y actuar en consecuencia. En el template `hello.html` podemos comprobar esa circunstancia y modificar la renderización según el usuario este logueado o no.

```jinja
<!-- hello.html -->

...
        <div class="row">
            <h1>Tutorial de Flask</h1>
            {% if username %}
                <h2>Bienvenido, {{ username }}</h2>
            {% endif %}
            {% if user_ip %}
            <h3 class="display-4">Hello World from Flask, tu IP es {{ user_ip }} </h3>
            {% else %}
            <a href="{{ url_for('index') }}">Ir a inicio</a>
            {% endif %}
        </div>
...
```

Pero para que esto funcione debemos añadir al contexto la variable `username` y realizar una redirección a la página deseada una vez el usuario se ha logado.

```python

...

@app.route('/hello', methods=['POST', 'GET'])
def hello():
    user_ip = session.get('user_ip')
    login_form = LoginForm()
    username = session.get('username')

    context = {
        'user_ip': user_ip,
        'todos': todos,
        'login_form': login_form,
        'username': username
    }

    if login_form.validate_on_submit():
        username = login_form.username.data
        session['username'] = username

        return redirect(url_for('index'))

    return render_template('hello.html', **context)
```

## Filtros Jinja

En los templates podemos usar filtros ya sea los incluidos (builtin) o personalizados. En la [documentación](https://jinja.palletsprojects.com/en/2.11.x/templates/?highlight=filters#builtin-filters) tienes ejemplos y una lista de todos ellos. Aquí tienes un ejemplo de convertir a mayúsculas.

```jinja
<!-- hello.html -->

...
        <div class="row">
            <h1>Tutorial de Flask</h1>
            {% if username %}
            <h2>Bienvenido, {{ username | upper }}</h2>
            {% endif %}
            {% if user_ip %}
            <h3 class="display-4">Hello World from Flask, tu IP es {{ user_ip }} </h3>
            {% else %}
            <a href="{{ url_for('index') }}">Ir a inicio</a>
            {% endif %}
        </div>
...
```

## Mostrar mensajes al usuario, Flashing

Las buenas aplicaciones e interfaces de usuario informan al usuario de lo que va sucediendo. Flask proporciona una forma realmente sencilla de enviar comentarios a un usuario mediante el sistema de flashes. Los mensajes se almacenan como en una pila. Importamos `flash` y luego en el código añadimos los mensajes que serán renderizados en los templates.

Por ejemplo, vamos a dar información al usuario de que su formulario fue procesado correctamente. Tras incluir en el código la línea para añadir el mensaje al sistema de *flashing*.

```python

...

@app.route('/hello', methods=['POST', 'GET'])
def hello():
    user_ip = session.get('user_ip')
    login_form = LoginForm()
    username = session.get('username')

    context = {
        'user_ip': user_ip,
        'todos': todos,
        'login_form': login_form,
        'username': username
    }

    if login_form.validate_on_submit():
        username = login_form.username.data
        session['username'] = username

        flash('Nombre de usuario registrado con éxito')

        return redirect(url_for('index'))

    return render_template('hello.html', **context)
```

En la plantilla `base.html` (para que puedan mostrarse los mensajes en todas las páginas) añadimos el código.

```jinja
<!-- base.html -->

...
{% block body %}
    {% block navbar %}
        {% include 'navbar.html' %}
    {% endblock navbar %}

    {% for message in get_flashed_messages() %}
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <strong>¡Atención!:</strong> {{ message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    {% endfor %}
...
```

 Quedó muy bonito pero no funciona, no podemos cerrar el mensaje. Esto es porque no hemos añadido los archivos de javascript que necesita esta utilidad de Bootstrap. Para añadirlos, hay que incluir al final de nuestro `base.html` un bloque `scripts` y hacer la llamada para que se incluyan los que tiene la plantilla padre de Bootstrap.

```jinja
<!-- base.html -->

...
    {% block content %}{% endblock content %}

    {% block scripts %}
        {{ super() }}
    {% endblock scripts %}
{% endblock body %}
```

## Pruebas básicas con Flask-Testing

Añadimos el requerimiento de la extensión `flask-testing` en nuestro `requirements.txt` e instalamos. Para correr las pruebas desde consola debemos crear un comando a tal fin. Flask nos proporciona un decorador `@cli.command()` para ello.

```python

...

from flask import flash
import unittest

app = Flask(__name__)
bootstrap = Bootstrap(app)

app.config['SECRET_KEY'] = 'CLAVE SECRETA'

todos = ['Comprar cafe',
         'Revisar bicicleta',
         'Reordenar biblioteca',
         'Llevar coche al taller']


class LoginForm(FlaskForm):
    username = StringField('Nombre de usuario', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Enviar')


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner().run(tests)
...
```

Ya podemos probar si nos funciona el comando. Ejecuta en la consola `flask test`.

```zsh
(flask_dev)
~/workspace/cursos/Flask on  master! ⌚ 19:36:35
$ flask test

----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
```

Funciona pero no tenemos ningún test preparado. Flask espera encontrar los tests en un directorio denominado `'tests'`, al menos eso es lo que le hemos dicho al crear el comando, que buscase *tests* en esa carpeta. Creemos un archivo en ella para contener los tests, `test_base.py`. Crearemos una clase que herede de `TestCase`, importado de `flask_testing` y que contenga los tests que vamos a ir definiendo.

```python
from flask_testing import TestCase
from flask import current_app, url_for
from main import app


class MainTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLE'] = False

        return app
```

Primero creamos nuestra aplicación a probar y la configuramos. Importamos la aplicación de `main.py` y configuramos el parámetro `'TESTING'` a `True` para indicar que estamos en pruebas. Como vamos a probar formularios, tenemos que establecer el **CSRF** (**C**ross **S**ite **R**equest **F**orgery), es decir, la protección contra ataques cruzados de otros webs distintos al nuestro, a desactivado. Esto es porque al hacer las pruebas `flask-testing` no es capaz de obtener el **CSRF** y esto se puede hacer de forma segura ya que lo usaremos mientras realizamos las pruebas. El parámetro a desactivar es `WTF_CSRF_ENABLED`, puesto a `False`.

Lo primero verificaremos que existe y está en marcha, la aplicación sobre la que vamos a realizar las pruebas. Nos apoyamos en `current_app` de `flask` que nos devuelve la aplicación que esta en ejecución.

```python
from flask_testing import TestCase
from flask import current_app, url_for
from main import app


class MainTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLE'] = False

        return app

    def test_app_exits(self):
        self.assertIsNotNone(current_app)
```

Prueba los tests. la consola te mostrará algo como lo siguiente.

```zsh
(flask_dev)
~/workspace/cursos/Flask on  master! ⌚ 19:36:35
$ flask test
.
----------------------------------------------------------------------
Ran 1 test in 0.002s

OK
```

Hemos añadido otros cuatro test para probar:

1. que estamos en modo de pruebas, `test_app_in_test_mode`
2. que cuando accedemos a la dirección `'index'` (`'/'`) somos redirigidos a `'/hello'`.
3. que cuando accedemos (métodO `'GET'`) a la dirección `'/hello'` no hay errores, esto es, nos devuelve un código 200 si la página carga correctamente.
4. que al enviar un `'POST'`  con un diccionario de datos que contenga un `username` y `password`, si todo ha ido bien,somos redirigidos a `'index'`.

A continuación el código de todos los test.

```python
from flask_testing import TestCase
from flask import current_app, url_for

from main import app


class MainTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False

        return app

    def test_app_exists(self):
        self.assertIsNotNone(current_app)

    def test_app_in_test_mode(self):
        self.assertTrue(current_app.config['TESTING'])

    def test_index_redirects(self):
        response = self.client.get(url_for('index'))

        self.assertRedirects(response, url_for('hello'))

    def test_hello_get(self):
        response = self.client.get(url_for('hello'))

        self.assert200(response)

    def test_hello_post(self):
        fake_form = {
            'username': 'fake',
            'password': 'fake-password'
        }
        response = self.client.post(url_for('hello'), data=fake_form)

        self.assertRedirects(response, url_for('index'))
```

Al ejecutar el comando `flask test` debes obtener algo como lo siguiente.

```zsh
(flask_dev)
~/workspace/cursos/Flask on  master! ⌚ 20:19:54
$ flask test
.....
----------------------------------------------------------------------
Ran 5 tests in 0.091s

OK
```
